import numpy
import heapq
from operator import itemgetter

class IsomapObject:
	name = None
	pos = None
	neighbors = None
	euclidd = None
	geodesicd = None

	def __init__(self, name, pos=None):
		if (pos is not None):
			self.pos = pos.flatten()
		self.name = name
		self.neighbors = set()
		self.euclidd = {}
		self.geodesicd = {}

	def getPos(self):
		return self.pos


class ObjectSet:
	objectsGraph = None

	def __init__(self):
		self.objectsGraph = {}

	def addObject(self, name, pos):
		if (name not in self.objectsGraph):
			self.objectsGraph[name] = IsomapObject(name, pos)
			return True
		else:
			return False

	def addObjects(self, distancemap):
		for ii in range(distancemap.shape[0]):
			self.objectsGraph[ii] = IsomapObject(ii)
			for jj in range(distancemap.shape[1]):
				self.objectsGraph[ii].euclidd[jj] = distancemap[ii, jj]

	def getObjectNames(self):
		return self.objectsGraph.keys()

	def getObject(self, name):
		return self.objectsGraph[name]

	def getObjectDeltas(self):
		out = numpy.empty((len(self.objectsGraph), len(self.objectsGraph)))
		for ii in range(len(self.objectsGraph)):
			for jj in range(ii+1):
				out[ii, jj] = self.delta(ii, jj)
			out[:ii, ii] = out[ii, :ii]
		return out

	def findNeighborhoods(self, avgNeighbors, clear=True):
		edges = []
		inGraph = set()
		inGraph.add(self.objectsGraph.keys()[0])

		# generate a list of edges
		# and clear old data if necessary
		for ii in self.objectsGraph:
			if clear:
				self.objectsGraph[ii].neighbors.clear()
				self.objectsGraph[ii].geodesicd.clear()
			for jj in self.objectsGraph:
				if (ii == jj):
					continue
				edgeLen = self.euclidd(ii, jj)
				edges.append((ii, jj, edgeLen))
		edges.sort(key=itemgetter(2))

		# add all edges less than a target to ensure an average number of neighbors
		tgtEdge = len(self.objectsGraph) * avgNeighbors
		tgtEdgeLen = edges[min(tgtEdge, len(edges) -1)][2]

		# now a modified prim, adding all edges less than the target edge length
		ii = 0
		while ii < len(edges):
			thisEdge = edges[ii]
			if (thisEdge[0] in inGraph and thisEdge[1] not in inGraph):
				inGraph.add(thisEdge[1])
				ii = 0
			elif (thisEdge[1] in inGraph and thisEdge[0] not in inGraph):
				inGraph.add(thisEdge[0])
				ii = 0
			elif (thisEdge[0] in inGraph and thisEdge[1] in inGraph):
				edges.pop(ii)
				if (thisEdge[2] > tgtEdgeLen and len(inGraph) == len(self.objectsGraph)):
					break
				elif (thisEdge[2] > tgtEdgeLen):
					continue
			else:
				ii += 1
				continue

			self.objectsGraph[thisEdge[0]].neighbors.add(thisEdge[1])
			self.objectsGraph[thisEdge[1]].neighbors.add(thisEdge[0])


	def euclidd(self, ob1, ob2):
		if (ob2 in self.objectsGraph[ob1].euclidd):
			return self.objectsGraph[ob1].euclidd[ob2]
		else:
			dist = numpy.sqrt( numpy.sum( numpy.power((self.objectsGraph[ob1].pos - self.objectsGraph[ob2].pos),2)) )
			self.objectsGraph[ob1].euclidd[ob2] = dist
			self.objectsGraph[ob2].euclidd[ob1] = dist
			return dist

	def AStar(self, ob1, ob2):
		closedSet = set()
		openSet = set([ob1])
		openQueue = [(0.0, ob1)]
		came_from = {ob1:None}
		weights = {ob1:0}
		currentnode = None

		while (len(openSet) > 0):
			(prio, currentnode) = heapq.heappop(openQueue)
			openSet.remove(currentnode)
			if (currentnode == ob2):
				break

			closedSet.add(currentnode)
			for neighbor in self.objectsGraph[currentnode].neighbors:
				if neighbor in closedSet:
					continue
				new_weight = weights[currentnode] + self.euclidd(currentnode, neighbor)
				if (neighbor not in openSet or new_weight < weights[neighbor]):
					came_from[neighbor] = currentnode
					weights[neighbor] = new_weight
					expected_cost = new_weight + self.euclidd(neighbor, ob2)
					if (neighbor not in openSet):
						heapq.heappush(openQueue, (expected_cost, neighbor))
						openSet.add(neighbor)
					else:
						for ii in range(len(openQueue)):
							if (openQueue[ii][1] == neighbor):
								openQueue[ii] = (expected_cost, neighbor)
								break
						heapq.heapify(openQueue)

		res = []
		while (currentnode != None):
			res.insert(0, currentnode)
			currentnode = came_from[currentnode]
		return res

	def geodesicd(self, ob1, ob2):
		if (ob2 in self.objectsGraph[ob1].geodesicd):
			return self.objectsGraph[ob1].geodesicd[ob2]
		else:
			path = self.AStar(ob1, ob2)
			pathsum = 0
			for ii in range(len(path) - 1):
				pathsum += self.euclidd(path[ii], path[ii+1])
			self.objectsGraph[ob1].geodesicd[ob2] = pathsum
			self.objectsGraph[ob2].geodesicd[ob1] = pathsum
			return pathsum

	def delta(self, ob1, ob2):
		return (self.euclidd(ob1, ob2) + self.geodesicd(ob1, ob2))/2.0

if __name__ == "__main__":
	dims = 2
	dimrange = (0,100)
	pointcount = 400
	avgNeighbors = 4

	import numpy.random
	import matplotlib.pyplot as plt
	import matplotlib.lines
	fig, ax = plt.subplots()
	fig.set_size_inches(6,6)

	oset = ObjectSet()
	for ii in range(pointcount):
		oset.addObject(ii, numpy.random.rand(dims) * (dimrange[1] - dimrange[0]) + dimrange[0])
	oset.findNeighborhoods(avgNeighbors)
	for ii in range(len(oset.objectsGraph)):
		plt.plot(oset.objectsGraph[ii].pos[0], oset.objectsGraph[ii].pos[1], "ko")
		for jj in range(ii+1, len(oset.objectsGraph)):
			if (jj in oset.objectsGraph[ii].neighbors):
				plt.plot((oset.objectsGraph[ii].pos[0], oset.objectsGraph[jj].pos[0]), (oset.objectsGraph[ii].pos[1], oset.objectsGraph[jj].pos[1]), "k-")

	plt.show()






